# Makefile

.PHONY: setup activate run

OS := $(shell uname)

setup:
ifeq ($(OS), Linux)
	pip3 install virtualenv
	virtualenv env
	. env/bin/activate && pip3 install -r requirements.txt
else
ifeq ($(OS), Darwin)
	pip3 install virtualenv
	virtualenv env
	. env/bin/activate && pip3 install -r requirements.txt
else
ifeq ($(findstring MSYS,$(OS)), MSYS)
	pip3 install virtualenv
	virtualenv env
	. env/Scripts/activate && pip3 install -r requirements.txt
else
	@echo "Unsupported operating system"
endif
endif
endif

activate:
ifeq ($(OS), Linux)
	echo "Linux"
	. env/bin/activate
else
ifeq ($(OS), Darwin)
	. env/bin/activate
else
ifeq ($(findstring MSYS,$(OS)), MSYS)
	. env/Scripts/activate
else
	@echo "Unsupported operating system"
endif
endif
endif

run: activate
	python3 main.py
