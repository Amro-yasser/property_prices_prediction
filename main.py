# Import necessary libraries
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import StandardScaler
from sklearn.impute import SimpleImputer
import matplotlib.pyplot as plt
from matplotlib.ticker import ScalarFormatter,FormatStrFormatter


# Load the dataset
df = pd.read_csv('sample_report_cleaned.csv')



# Display the first few rows of the dataset to understand its structure
print(df.head())

#setting the target variable
y = df['Evaluation Price']

# Features: 'Land Value', 'Construction Cost', 'Area size', 'Evaluation date'
X = df[['Land Value', 'Construction Cost', 'Area size', 'Evaluation date']]

# Data preprocessing: Extract relevant information from 'Evaluation date'
X['Evaluation Year'] = pd.to_datetime(X['Evaluation date']).dt.year
X['Evaluation Month'] = pd.to_datetime(X['Evaluation date']).dt.month
X.drop(['Evaluation date'], axis=1, inplace=True)

# Impute missing values
imputer = SimpleImputer(strategy='mean')  
X_imputed = pd.DataFrame(imputer.fit_transform(X), columns=X.columns)

# Scale numerical features
scaler = StandardScaler()
X_scaled = scaler.fit_transform(X_imputed)

# Split the dataset into training and testing sets
X_train, X_test, y_train, y_test = train_test_split(X_scaled, y, test_size=0.2, random_state=42)

# Create a Linear Regression model
model = LinearRegression()

# Train the model
model.fit(X_train, y_train)

# Make predictions on the test set
predictions = model.predict(X_test)

# Evaluate the model
mse = mean_squared_error(y_test, predictions)
print(f'Mean Squared Error: {mse}')

# Now, let's predict prices for multiple properties
# Example values for features of multiple properties

lands_values = df['Land Value']
construction_costs = df['Construction Cost']
areas_sizes = df['Area size']
evaluation_dates = df['Evaluation date']


new_data = pd.DataFrame({
    'Land Value': lands_values,
    'Construction Cost': construction_costs,
    'Area size': areas_sizes,
    'Evaluation date': evaluation_dates,
})

# Extract relevant information from 'Evaluation date'
new_data['Evaluation Year'] = pd.to_datetime(new_data['Evaluation date']).dt.year
new_data['Evaluation Month'] = pd.to_datetime(new_data['Evaluation date']).dt.month
new_data.drop(['Evaluation date'], axis=1, inplace=True)

# Impute missing values for the new data
new_data_imputed = pd.DataFrame(imputer.transform(new_data), columns=new_data.columns)

# Scale the new data using the same scaler
new_data_scaled = scaler.transform(new_data_imputed)

# Make predictions for all properties
predicted_prices = model.predict(new_data_scaled)

# Display the predicted prices for each property
for i, predicted_price in enumerate(predicted_prices):
    print(f'Predicted Price for Property {i+1}: {predicted_price}')


property_names = [f'Property {i+1}' for i in range(len(predicted_prices))]

# Plotting 10 first properties predicted prices
plt.stem(property_names[:10], predicted_prices[:10])

plt.xlabel('Properties',)
plt.ylabel('Predicted Prices : AED')
plt.title('Predicted Prices for 10 first Properties')
rounded_predicted_prices = [round(price, 2) for price in predicted_prices]
plt.gca().yaxis.set_major_formatter(FormatStrFormatter('%.0f aed'))

plt.show()