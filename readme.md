# Project Setup

## Setup Instructions

### 0. Clone the Repository
first of all clone the repository by running the following command:

    ``` git clone https://gitlab.com/Amro-yasser/property_prices_prediction.git ```

### 1. Set up a Virtual Environment and Install Dependencies

Before running the application, set up a virtual environment to isolate dependencies and install them :
    
    ``` make setup ```
it will create a virtual environment and install all the dependencies.

### 2. Activate the Virtual Environment
if you already installed an activated the virtual environment then skip this step. But if you are using the virtual environment for the first time then activate it by running the following command:

    ``` make activate ```

### 3. Run the Application
to run the application run the following command:

    ``` make run ```
